# histoire 展示组件

编写组件，还是有文档和例子，别人用起来才方便。如何编写文档和例子，是一个非常重要的问题。

[histoire](https://histoire.dev/) 提供了很好的解决方案。它是一个类似 [storybook](https://storybook.js.org/) 文档和例子生成工具，还提供组件UI测试，效果展示、代码复制等功能。当你为组件编写完毕使用例子的时候，文档、交互性例子、关键的 UI 测试等工作也就就完成了。

## 快速开始

```bash
pmpm i -D histoire @histoire/plugin-vue
```

配置 histoire

1. 在根目录在新建`histoire.config.ts`文件，内容如下：

```ts
import { defineConfig } from 'histoire'
import { HstVue } from '@histoire/plugin-vue'

export default defineConfig({
  setupFile: '/src/histoire.setup.ts',
  plugins: [HstVue()],
})
```

2. 在`src`目录下新建`histoire.setup.ts`文件，内容如下：

```ts
import 'uno.css' // 组件的样式用到了 uno.css 
```

setupFile 是类似项目的`main`入口文件，可以在这里引入全局样式、框架样式、pinia插件、全局组件等。

[更多信息](https://histoire.dev/guide/config.html#global-js-and-css)

3. 编写一个 Button 组件的 story

`Button.stroy.vue`

```html
<script setup>
import { Button } from './Button'
</script>

<template>
  <Story color="颜色"> 
    <Button color="blue"/>
    <Button color="red"/>
    <Button color="gray"/>
  </Story>
</template>

<docs lang="md">
  # EcButton 按钮组件
  
</docs>
```
4. 启动脚本

```json
{
  "scripts": {
    "story:dev": "histoire dev",
    "story:build": "histoire build",
    "story:preview": "histoire build && histoire preview"
  }
}
```

执行`npm run story:dev`，查看环境是否可用。

当看到类似的页面，说明环境搭建成功。

![histoire启动页面](https://jsd.cdn.zzko.cn/gh/jackchoumine/jack-picture@master/histoire/histore_get_started.png)


## 其他编写文档的工具

- [Vue Styleguidist](https://vue-styleguidist.github.io/docs/Documenting.html#code-comments)
- [Docz](https://v2.docusaurus.io/docs/)

## 更多参考

[Storybook的基本使用指南](https://ths.js.org/2020/09/27/storybook%E7%9A%84%E5%9F%BA%E6%9C%AC%E4%BD%BF%E7%94%A8%E6%8C%87%E5%8D%97/#1-%E4%BB%80%E4%B9%88%E6%98%AF-Storybook)
