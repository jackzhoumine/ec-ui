/*
 * @Author      : ZhouQiJun
 * @Date        : 2024-05-12 20:45:23
 * @LastEditors : ZhouQiJun
 * @LastEditTime: 2025-02-24 17:05:43
 * @Description :
 */
import { presetUno, presetAttributify, presetIcons } from 'unocss'
import Unocss from 'unocss/vite'

const colors = [
  'white',
  'black',
  'gray',
  'red',
  'yellow',
  'green',
  'blue',
  'indigo',
  'purple',
  'pink',
]

const icons = ['search', 'edit', 'check', 'message', 'star-off', 'delete', 'add', 'share']
const safelist = [
  ...colors.map(v => `bg-${v}-500`),
  ...colors.map(v => `hover:bg-${v}-700`),
  ...icons.map(v => `i-ic-baseline-${v}`),
]

function unocssConfig() {
  return Unocss({
    safelist,
    presets: [presetUno(), presetAttributify(), presetIcons()],
  })
}

export default unocssConfig
