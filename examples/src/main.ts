/*
 * @Author      : ZhouQiJun
 * @Date        : 2025-02-24 17:08:53
 * @LastEditors : ZhouQiJun
 * @LastEditTime: 2025-02-24 17:30:03
 * @Description : 入口文件
 */

import { createApp } from 'vue'
import { createPinia } from 'pinia'

import App from './App.vue'
import router from './router'

//import './assets/main.css'
import '../../dist/style.css'
// import 'uno.css'

const app = createApp(App)

app.use(createPinia())
app.use(router)

app.mount('#app')
