/*
 * @Author      : ZhouQiJun
 * @Date        : 2025-02-24 17:08:53
 * @LastEditors : ZhouQiJun
 * @LastEditTime: 2025-02-24 17:30:40
 * @Description : vite 配置
 */
import { fileURLToPath, URL } from 'node:url'

import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import vueJsx from '@vitejs/plugin-vue-jsx'
//import unocss from '../config/unocss.js'
//import vueDevTools from 'vite-plugin-vue-devtools'

// https://vite.dev/config/

export default defineConfig({
  plugins: [
    vue(),
    vueJsx(),
    //unocss(),
    //vueDevTools(),
  ],
  resolve: {
    alias: {
      '@': fileURLToPath(new URL('./src', import.meta.url)),
    },
  },
})
