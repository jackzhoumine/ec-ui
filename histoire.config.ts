/*
 * @Author      : ZhouQiJun
 * @Date        : 2024-05-14 02:23:11
 * @LastEditors : ZhouQiJun
 * @LastEditTime: 2024-05-14 02:35:04
 * @Description : histoire 配置文件
 */
import { defineConfig } from 'histoire'
import { HstVue } from '@histoire/plugin-vue'

export default defineConfig({
  setupFile: '/src/histoire.setup.ts',
  plugins: [HstVue()],
})
