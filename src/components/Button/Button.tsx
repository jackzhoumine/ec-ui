/*
 * @Author      : ZhouQiJun
 * @Date        : 2024-05-12 16:54:15
 * @LastEditors : ZhouQiJun
 * @LastEditTime: 2025-02-24 17:34:21
 * @Description : 按钮组件
 */
import { defineComponent } from 'vue'
import type { PropType } from 'vue'

// 颜色类型声明
export type IColor =
  | 'black'
  | 'gray'
  | 'red'
  | 'yellow'
  | 'green'
  | 'blue'
  | 'indigo'
  | 'purple'
  | 'pink'

export const props = {
  /**
   * @description: 按钮颜色
   * @default blue
   * @type {IColor}
   * @enum ['black', 'gray', 'red', 'yellow', 'green', 'blue', 'indigo', 'purple', 'pink']
   * @example <es-button color="red">按钮</es-button>
   * @example <es-button color="yellow">按钮</es-button>
   *
   */
  color: {
    type: String as PropType<IColor>,
    default: 'blue', // 设定默认颜色
  },
  /**
   * @description: 按钮图标
   */
  icon: {
    type: String,
    default: '',
  },
}

export const Button = defineComponent({
  name: 'EcButton',
  props,
  setup(props, { slots }) {
    const classList = `
    py-2
    px-4
    font-semibold
    rounded-lg
    shadow-md
    text-white
    bg-${props.color}-500
    hover:bg-${props.color}-700
    border-none
    cursor-pointer
    mx-1
    `
    return () => (
      <button class={classList}>
        {props.icon !== '' ? <i class={`i-ic-baseline-${props.icon} p-3`}></i> : ''}
        {slots.default ? slots.default() : ''}
      </button>
    )
  },
})
