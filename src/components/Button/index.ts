/*
 * @Author      : ZhouQiJun
 * @Date        : 2024-05-12 18:32:47
 * @LastEditors : ZhouQiJun
 * @LastEditTime: 2024-05-12 22:04:50
 * @Description :
 */
import type { App, Plugin } from 'vue'
import { Button } from './Button'

Button.install = (app: App) => {
  if (Button.installed) return app
  app.component('EcButton', Button)
  Button.installed = true
  return app
}

export default Button as typeof Button & Plugin
