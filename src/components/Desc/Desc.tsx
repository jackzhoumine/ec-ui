/*
 * @Author      : ZhouQiJun
 * @Date        : 2024-05-14 00:22:14
 * @LastEditors : ZhouQiJun
 * @LastEditTime: 2024-05-14 09:59:16
 * @Description : 描述组件
 */
import { computed, defineComponent, ref, Ref, render } from 'vue'
// import Container from './container'
// import { copyText } from './utils/index'
// import clone from 'clone'
import { RenderContainer } from './RenderContainer'

import './Desc.scss'

interface Col {
  label: string | (() => string)
  prop: string | (() => string)
  span?: number
}
interface Props {
  title: string
  labelWidth: number
  sizeOneRow: number
  cols: Col[]
  data: Record<string, any>
}

export const Desc = defineComponent({
  name: 'EcDesc',
  components: {
    // RenderContainer,
  },
  props: {
    title: {
      type: String,
      default: '',
    },
    labelWidth: {
      type: Number,
      default: 120,
    },
    sizeOneRow: {
      type: Number,
      default: 3,
      validator: (value: number) => {
        const validate = [1, 2, 3, 4, 5, 6].includes(value)
        if (!validate) {
          console.error('titleNumPreRow 表示一行有标题字段对,只能时 1 -- 6 的偶数,默认 3')
        }
        return validate
      },
    },
    cols: {
      type: Array,
      default: () => [],
      validator: (value: Col[]) => {
        const validate = value.every(item => item.label && item.prop)
        if (!validate) {
          console.log('传入的 cols 数组中，元素的属性必须包含 label  和 prop 属性')
        }
        return validate
      },
    },
    data: {
      type: Object,
      default: () => ({}),
    },
  },
  setup(props: Props, { slots }) {
    console.log('props', props)
    const _cols = computed(() => {
      // NOTE 使用 JSON.stringify 深度复制丢失方法,此处勿用
      // const titleInfo = JSON.parse(JSON.stringify(this.titleList))
      const titleInfo = props.cols //clone(this.titleList)
      console.log('titleInfo', titleInfo)
      if (titleInfo.every(item => !!item.span)) {
        // NOTE 如何用户有设置每个标题的宽度, 就不适配最后一个
        return titleInfo
      }
      const titleNumPreRow = props.sizeOneRow
      const remainder = titleInfo.length % titleNumPreRow
      if (1 === remainder) {
        titleInfo[titleInfo.length - 1].span = titleNumPreRow
      }
      if (1 < remainder && remainder < titleNumPreRow) {
        titleInfo[titleInfo.length - 1].span = titleNumPreRow - remainder + 1
      }
      return titleInfo
    })
    const hasTitleSlot = computed(() => !!slots.label)

    const setTile = (el: HTMLElement, titleValue: string | (() => string)) => {
      const textContent = el.textContent
      const title = textContent.trim() || '暂无数据'
      el.title = typeof titleValue === 'string' ? titleValue : title
    }

    return () => {
      const Title = hasTitleSlot.value ? (
        <div class='title-box'>{slots.title()}</div>
      ) : props.title ? (
        <div class='title-box'>
          <span class='title-text'>{props.title}</span>
        </div>
      ) : null

      const Empty = <div class='no-data'>暂无数据</div>

      return (
        <div class='ec-desc'>
          <Title />
          {props.cols.length > 0 ? renderUl(_cols.value) : <Empty />}
        </div>
      )

      function renderUl(cols: Col[]) {
        return <ul>{cols.map(renderLi)}</ul>
      }

      function renderLi(col: Col) {
        return (
          <li
            style={{
              width: ((col.span || 1) / props.sizeOneRow) * 100 + '%',
            }}>
            {renderLabel(col)}
            {renderValue(col)}
          </li>
        )
      }

      function renderLabel(col: Col) {
        if (typeof col.label === 'function') {
          return (
            <div class='label-box' style={`width: ${props.labelWidth}px;`}>
              <RenderContainer render={col.label} data={props.data} />
            </div>
          )
        }
        return (
          <div class='label-box' style={`width: ${props.labelWidth}px;`}>
            <span>{col.label}</span>
          </div>
        )
      }

      function renderValue(col: Col) {
        if (typeof col.prop === 'function') {
          return (
            <div
              class='value-box'
              style={`width:calc(100% - ${props.labelWidth}px);`}>
              <RenderContainer render={col.prop} data={props.data} />
            </div>
          )
        }
        return (
          <div
            class='value-box'
            style={`width:calc(100% - ${props.labelWidth}px);`}>
            <span>
              {col.prop && ![null, undefined].includes(props.data[col.prop])
                ? props.data[col.prop]
                : '--'}
            </span>
          </div>
        )
      }
    }
  },
})
