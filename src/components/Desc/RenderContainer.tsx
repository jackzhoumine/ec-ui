/*
 * @Author      : ZhouQiJun
 * @Date        : 2024-05-14 00:40:55
 * @LastEditors : ZhouQiJun
 * @LastEditTime: 2024-05-14 09:53:27
 * @Description : 渲染容器组件
 */
import type { VNode } from 'vue'
// NOTE vue3 的 函数式组件如何编写
// https://vuejs.org/guide/extras/render-function#functional-components
export type RenderFn = (data: object, key?: string) => VNode

// type Props = { render: RenderFn; data: object; key?: string }
export function RenderContainer(props) {
  return props.render?.(props.data)
}

// RenderContainer.name = 'RenderContainer'

RenderContainer.props = {
  render: {
    type: Function,
    required: true,
  },
  data: {
    type: Object,
    default: () => ({}),
  },
}
