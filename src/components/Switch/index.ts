/*
 * @Author      : ZhouQiJun
 * @Date        : 2024-05-12 18:37:56
 * @LastEditors : ZhouQiJun
 * @LastEditTime: 2024-05-12 22:04:35
 * @Description :
 */
import type { App, Plugin } from 'vue'

import Switch from './Switch.vue'

Switch.install = (app: App) => {
  if (Switch.installed) return app
  app.component('EcSwitch', Switch)
  Switch.installed = true
  return app
}

export default Switch as typeof Switch & Plugin
