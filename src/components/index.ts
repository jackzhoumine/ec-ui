/*
 * @Author      : ZhouQiJun
 * @Date        : 2024-05-12 16:57:45
 * @LastEditors : ZhouQiJun
 * @LastEditTime: 2025-02-24 17:35:27
 * @Description : 导出所有组件
 */
import Button from './Button'
import Switch from './Switch'
import Desc from './Desc'

import 'uno.css'

const components = [Button, Switch, Desc]

function install(app) {
  components.forEach(component => {
    if (component.install && !component.installed) {
      // 有 install 方法的组件，调用 install 方法 注册组件
      app.use(component)
    } else {
      // 没有 install 方法的组件，直接注册组件
      app.component(component.name, component)
      component.installed = true
    }
  })
  return app
}

/**
 * 按需引入
 * import { Button } from "es-ui"
 * app.use(Button)
 */
export { Button, Switch }

/**
 * 全局引入
 * import ESUI from "es-ui"
 * app.use(ESUI)
 */
export default {
  install,
}
