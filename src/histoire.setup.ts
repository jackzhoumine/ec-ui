/*
 * @Author      : ZhouQiJun
 * @Date        : 2024-05-14 02:35:22
 * @LastEditors : ZhouQiJun
 * @LastEditTime: 2024-05-14 03:02:45
 * @Description :
*/
import { defineSetupVue3 } from '@histoire/plugin-vue'
import 'uno.css'

export const setupVue3 = defineSetupVue3(({ app, story, variant }) => {
  //   console.log('setupVue3', app, story, variant)
  //   const pinia = createPinia()
  //   app.use(pinia) // Add Pinia store
})