/*
 * @Author      : ZhouQiJun
 * @Date        : 2024-05-12 16:35:14
 * @LastEditors : ZhouQiJun
 * @LastEditTime: 2024-05-14 01:01:02
 * @Description :
 */
import { createApp } from 'vue'

import Demos from './Demos.vue'
import ESUI from './components'
// import { Switch } from './components'
// console.log(ESUI)
const app = createApp(Demos)

// app.use(Switch)
app.use(ESUI)

app.mount('#app')
