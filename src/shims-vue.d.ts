/*
 * @Author      : ZhouQiJun
 * @Date        : 2024-05-12 18:44:48
 * @LastEditors : ZhouQiJun
 * @LastEditTime: 2024-05-12 18:45:02
 * @Description : vue 单文件组件类型声明
 */
declare module '*.vue' {
  import { DefineComponent } from 'vue'
  const component: DefineComponent<{}, {}, any>
  export default component
}
