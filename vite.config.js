/*
 * @Author      : ZhouQiJun
 * @Date        : 2024-05-12 17:09:03
 * @LastEditors : ZhouQiJun
 * @LastEditTime: 2024-05-13 01:35:07
 * @Description : vite 配置文件
 */
import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import vueJsx from '@vitejs/plugin-vue-jsx'
import unocssConfig from './config/unocss'


const rollupOptions = {
  external: ['vue'],
  output: {
    globals: {
      vue: 'Vue',
    },
  },
}

export default defineConfig({
  plugins: [
    vue(),
    vueJsx(),
    // 添加UnoCSS插件
    unocssConfig(),
  ],
  build: {
    rollupOptions,
    minify: 'terser', // 使用 terser 压缩代码
    reportCompressedSize: true, // 显示压缩后的文件大小
    sourcemap: true,
    // 默认情况下，Vite 会将所有 CSS 提取到一个单独的 CSS 文件中，这样可以确保最终的生产环境中只有一个 CSS 文件。
    // cssCodeSplit: true,
    lib: {
      entry: './src/components/index.ts',
      name: 'ECUI',
      fileName: format => {
        if (format === 'es') return `ecui.js`
        if (format === 'cjs') return `ecui.cjs`
        return `ecui.${format}.js`
      },
      formats: ['es', 'umd', 'cjs'],
    },
  },
  server: {
    open: true,
  },
})
